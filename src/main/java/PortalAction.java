import org.bukkit.event.player.PlayerMoveEvent;

public interface PortalAction
{
    public void run(PlayerMoveEvent event);
}
