import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

abstract public class Portal implements PortalAction
{
    World world;
    double  x1, y1, z1, x2, y2, z2;

    public Portal(Location loc1, Location loc2) {
        this(loc1.getWorld(), loc1.getX(), loc1.getY(), loc1.getZ(), loc2.getX(), loc2.getY(), loc2.getZ());

        if(!loc1.getWorld().getName().equals(loc2.getWorld().getName()))
            throw new IllegalArgumentException("Locations must be in the same world");
    }

    public Portal(String world, double x1, double y1, double z1, double x2, double y2, double z2) {
        this(Bukkit.getWorld(world), x1, y1, z1, x2, y2, z2);
    }

    public Portal(World world, double x1, double y1, double z1, double x2, double y2, double z2) {
        this.world = world;
        this.x1 = x1;
        this.y1 = y1;
        this.z1 = z1;
        this.x2 = x2;
        this.y2 = y2;
        this.z2 = z2;

        PortalManager.portals.add(this);
    }
}
