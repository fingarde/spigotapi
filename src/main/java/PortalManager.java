import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.LinkedList;

public class PortalManager implements Listener
{
    static LinkedList<Portal> portals;
    public PortalManager(JavaPlugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);

        portals = new LinkedList<Portal>();
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        portals.forEach(portal -> {
            portal.run(event);
        });
    }

}
